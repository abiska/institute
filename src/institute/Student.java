package institute;

public class Student {
    
    //attributes
    private String name;
    private int id;

    //constructor(s)
    public Student(String name, int id) {
        this.name = name;
        this.id = id;
    }
    
    //getter(s)
    public String getName() {
        return name;
    }
    public int getId() {
        return id;
    }
}
