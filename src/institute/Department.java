package institute;

import java.util.List;


public class Department {
    
    //attributes
    private String name;
    private List<Student> students;
    
    //constructor
    public Department(String name, List<Student> students) {
        this.name = name;
        this.students = students;
    }
    
    //getters
    public String getName() {
        return name;
    }
    public List<Student> getStudents() {
        return students;
    } 
}
