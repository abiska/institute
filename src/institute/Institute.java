package institute;

import java.util.List;

public class Institute {
    
    //attributes
    private String name;
    private List<Department> departments;
    
    //constructor(s)
    public Institute(String name, List<Department> departments) {
        this.name = name;
        this.departments = departments;
    }
    
    //getter(s)
    public String getName() {
        return name;
    }
    public List<Department> getDepartments() {
        return departments;
    }
    
    
    //method to count all students in the institute
    public int getTotalStudentsInstitute(){
    
        int numOfStudents = 0;
        List<Student> students;
        
        for(Department dept: departments){
            students = dept.getStudents();
            
            for(Student s : students){
                numOfStudents++;
            }
        }
        
        return numOfStudents;
    }
}
