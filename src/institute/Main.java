package institute;

import java.util.*;    


public class Main {


    public static void main(String[] args) {
        
        Student s1 = new Student("Sherlock", 1);
        Student s2 = new Student("Holmes", 2);
        Student s3 = new Student("Watson", 3);
        
        List <Student> students = new ArrayList <Student>();
        students.add(s1);
        students.add(s2);
        students.add(s3);
        
        Department dept = new Department("HELLO", students);
        List <Department> departments = new ArrayList <Department>();
        departments.add(dept);
        
        Institute inst = new Institute("BTS", departments);
        
        System.out.println("Total students in institute: " + inst.getTotalStudentsInstitute());
        
        
    }
    
}
